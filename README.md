Todo-Server
===========
This repository contains the backend components of a simple CRUD application that allows the creation of users, each with their own list of TODOs.  These TODOs can contain a description, importance and/or due date in order to help a user organize and prioritize any items/activities/events/etc that need to be done.



### Information
This application uses a variety of components/technologies in order to perform its tasks.  JAX-RS is used to handle the requests and responses while JSON is used as the media type of choice for request/response data.  The JSON is consumed/produced via JAXB (Jackson) which allows the JSON to be tied into our model objects.  Any model objects (User or TODO) that needs to be saved is persisted via JPA to the database.



### Usage
Two web services are provided that various HTTP methods can be called against, [UserService](src/main/java/com/mitchellbdunn/todo/service/UserService.java) and [TodoService](src/main/java/com/mitchellbdunn/todo/service/TodoService.java).  UserService provides methods/actions to perform for [User objects](src/main/java/com/mitchellbdunn/todo/model/User.java) while TodoService provides methods/actions to perform against [Todo objects](src/main/java/com/mitchellbdunn/todo/model/Todo.java).

#### User Service
 - `GET /user`: gets all users
 - `GET /user/{id}`: gets user specified by id
 - `GET /user/{id}/todo`: gets all the todos of a user specified by the user id
 - `POST /user`: creates a new user
 - `POST /user/{id}/todo`: creates a todo for a user specified by the user id
 - `PUT /user/{id}`: updates a user
 - `DELETE /user/{id}`: deletes a user

#### Todo Service
 - `GET /todo`: gets all todos
 - `GET /todo/{id}`: gets todo specified by id
 - `POST /todo`: creates a new todo
 - `PUT /todo/{id}`: updates a todo
 - `DELETE /todo/{id}`: deletes a todo



### Database
To store the user information and TODOs needed for this application a specific database structure is required.  This structure and the way of creating it can be found in [this SQL script](src/main/resources/sql/createTables.sql).



### Security
The application uses an authentication filter to ensure that users are authenticated before calling services (the only API that requires no authentication is the createUser API found in UserService).  Services that require authentication must include the user/pass in a standard 'Authorization Basic' style HTTP header.  To ensure the safety of the credentials in transit, the [web.xml](src/main/webapp/WEB-INF/web.xml) for this application has a security constraint of CONFIDENTIAL for all URLS, meaning that all requests/responses must use HTTPS.
The credentials are stored in the database hashed via SHA-512 and then Base64 encoded; a salt is prepended to the password and stored in the database also Base64 encoded.  To authenticate the salt is prepended to the password and this String is hashed and the result compared to what is stored in the password column for the given user, if the username/password match the authentication is passed.  For more info see [AuthenticationFilter](src/main/java/com/mitchellbdunn/todo/util/AuthenticationFilter.java) and [SecurityUtil](src/main/java/com/mitchellbdunn/todo/util/SecurityUtil.java).
