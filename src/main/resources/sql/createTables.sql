CREATE TABLE users(
    id int primary key,
    username varchar(255),
    password varchar(255),
    salt varchar(255),
    first_name varchar(255),
    last_name varchar(255),
    email varchar(255),
    date_joined timestamp
);

CREATE SEQUENCE users_id_seq;

CREATE TABLE todo(
    id int primary key,
    name varchar(255),
    description varchar(255),
    importance varchar(255),
    date_created timestamp,
    date_due timestamp,
    user_id int
);

CREATE SEQUENCE todo_id_seq;