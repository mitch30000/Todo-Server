package com.mitchellbdunn.todo.util;

import com.mitchellbdunn.todo.dao.UserDao;
import com.mitchellbdunn.todo.model.User;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author Mitch Dunn
 */
public final class SecurityUtil {

    private static final UserDao dao = new UserDao();

    public static String generateSalt() {
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[8];
        random.nextBytes(salt);
        return new String(Base64.encodeBase64(salt));
    }

    public static String generateHash(String salt, String password) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex);
        }
        digest.reset();
        digest.update(Base64.decodeBase64(salt));
        byte[] input = digest.digest(password.getBytes());
        return new String(Base64.encodeBase64(input));
    }

    public static boolean authenticate(String username, String password) {
        User user = dao.readByUsername(username);
        if (user == null) {
            return false;
        }
        String hash = generateHash(user.getSalt(), password);
        return hash.equals(user.getPassword());
    }
}
