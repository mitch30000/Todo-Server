package com.mitchellbdunn.todo.util;

import com.mitchellbdunn.todo.dao.UserDao;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author Mitch Dunn
 */
public class AuthenticationFilter implements Filter {

    private UserDao dao;
    
    public void init(FilterConfig filterConfig) throws ServletException {
        dao = new UserDao();
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        // Let creation of users pass through
        if(req.getMethod().equalsIgnoreCase("POST") && req.getPathInfo().contains("/user")) {
            chain.doFilter(request, response);
            return;
        }
        String auth = req.getHeader("Authorization");
        if(auth != null && auth.startsWith("Basic")) {
            // Take the authorization string, remove the prepended 'Basic ', decode the
            // credentials and split them via ':'
            String[] credentials = new String(Base64.decodeBase64(auth.split(" ")[1])).split(":");
            boolean authenticated = SecurityUtil.authenticate(credentials[0], credentials[1]);
            if(authenticated) {
                chain.doFilter(request, response);
            } else {
                res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
            }
        } else {
            res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
        }
    }

    public void destroy() {
        dao = null;
    }
    
}
