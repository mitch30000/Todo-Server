package com.mitchellbdunn.todo.dao;

import com.mitchellbdunn.todo.model.User;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Mitch Dunn
 */
public class UserDao {

    private EntityManagerFactory factory;

    public int create(User entity) {
        EntityManager manager = getSession();
        manager.getTransaction().begin();
        manager.persist(entity);
        manager.getTransaction().commit();
        manager.close();
        return entity.getId();
    }

    public User readById(int id) {
        try {
            EntityManager manager = getSession();
            String queryString = "select u from User u where u.id=:id";
            Query query = manager.createQuery(queryString);
            query.setParameter("id", id);
            User result = (User) query.getSingleResult();
            manager.close();
            return result;
        } catch (Exception ex) {
            return null;
        }
    }
    
    public User readByUsername(String username) {
        try {
            EntityManager manager = getSession();
            String queryString = "select u from User u where u.username=:username";
            Query query = manager.createQuery(queryString);
            query.setParameter("username", username);
            User result = (User) query.getSingleResult();
            manager.close();
            return result;
        } catch (Exception ex) {
            return null;
        }
    }

    public List<User> readAll() {
        EntityManager manager = getSession();
        String queryString = "select u from User u";
        Query query = manager.createQuery(queryString);
        List<User> results = query.getResultList();
        manager.close();
        return results;
    }

    public boolean doesEntityExist(int id) {
        return readById(id) != null;
    }

    public void update(User entity) {
        EntityManager manager = getSession();
        manager.getTransaction().begin();
        manager.merge(entity);
        manager.getTransaction().commit();
        manager.close();
    }

    public void delete(int id) {
        User user = readById(id);
        delete(user);
    }

    public void delete(User entity) {
        EntityManager manager = getSession();
        manager.getTransaction().begin();
        manager.remove(manager.merge(entity));
        manager.getTransaction().commit();
        manager.close();
    }

    private EntityManager getSession() {
        if (factory == null) {
            factory = Persistence.createEntityManagerFactory("todo-pu");
        }
        return factory.createEntityManager();
    }
}
