package com.mitchellbdunn.todo.dao;

import com.mitchellbdunn.todo.model.Todo;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Mitch Dunn
 */
public class TodoDao {

    private EntityManagerFactory factory;

    public int create(Todo entity) {
        EntityManager manager = getSession();
        manager.getTransaction().begin();
        manager.persist(entity);
        manager.getTransaction().commit();
        manager.close();
        return entity.getId();
    }

    public Todo readById(int id) {
        try {
            EntityManager manager = getSession();
            String queryString = "select t from Todo t where t.id=:id";
            Query query = manager.createQuery(queryString);
            query.setParameter("id", id);
            Todo result = (Todo) query.getSingleResult();
            manager.close();
            return result;
        } catch (Exception ex) {
            return null;
        }
    }

    public List<Todo> readAll() {
        EntityManager manager = getSession();
        String queryString = "select t from Todo t";
        Query query = manager.createQuery(queryString);
        List<Todo> results = query.getResultList();
        manager.close();
        return results;
    }
    
    public boolean doesEntityExist(int id) {
        return readById(id) != null;
    }

    public List<Todo> readUsersTodoList(int userId) {
        EntityManager manager = getSession();
        String queryString = "select t from Todo t where t.owner.id=:id";
        Query query = manager.createQuery(queryString);
        query.setParameter("id", userId);
        List<Todo> results = query.getResultList();
        manager.close();
        return results;
    }

    public void update(Todo entity) {
        EntityManager manager = getSession();
        manager.getTransaction().begin();
        manager.merge(entity);
        manager.getTransaction().commit();
        manager.close();
    }

    public void delete(int id) {
        Todo todo = readById(id);
        delete(todo);
    }

    public void delete(Todo entity) {
        EntityManager manager = getSession();
        manager.getTransaction().begin();
        manager.remove(manager.merge(entity));
        manager.getTransaction().commit();
        manager.close();
    }

    private EntityManager getSession() {
        if (factory == null) {
            factory = Persistence.createEntityManagerFactory("todo-pu");
        }
        return factory.createEntityManager();
    }
}
