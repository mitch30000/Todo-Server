package com.mitchellbdunn.todo;

import com.mitchellbdunn.todo.service.TodoService;
import com.mitchellbdunn.todo.service.UserService;
import com.mitchellbdunn.todo.util.CorsResponseFilter;
import org.glassfish.jersey.server.ResourceConfig;

/**
 *
 * @author Mitch Dunn
 */
public class TodoApplication extends ResourceConfig {
    public TodoApplication() {
        register(TodoService.class);
        register(UserService.class);
        register(CorsResponseFilter.class);
    }
}
