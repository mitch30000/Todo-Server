package com.mitchellbdunn.todo.service;

import com.mitchellbdunn.todo.dao.TodoDao;
import com.mitchellbdunn.todo.dao.UserDao;
import com.mitchellbdunn.todo.model.Todo;
import com.mitchellbdunn.todo.model.User;
import com.mitchellbdunn.todo.util.SecurityUtil;
import java.net.URI;
import java.util.Date;
import java.util.List;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Mitch Dunn
 */
@Path("/user")
public class UserService {

    private final UserDao userDao;
    private final TodoDao todoDao;
    
    @Context
    UriInfo uriInfo;

    public UserService() {
        userDao = new UserDao();
        todoDao = new TodoDao();
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response readAllUsers() {
        List<User> users = userDao.readAll();
        return Response.ok(users).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response readUser(@PathParam("id") int id) {
        User user = userDao.readById(id);
        if (user == null) {
            throw new NotFoundException();
        }
        return Response.ok(user).build();
    }
 
    @GET
    @Path("/{id}/todo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response readUsersTodoList(@PathParam("id") int id) {
        User user = userDao.readById(id);
        if (user == null) {
            throw new NotFoundException();
        }
        List<Todo> todos = todoDao.readUsersTodoList(id);
        return Response.ok(todos).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createUser(User user) {
        User usernameCheck = userDao.readByUsername(user.getUsername());
        if(usernameCheck != null) {
            throw new BadRequestException("Username '" + user.getUsername() + "'already taken");
        }
        user.setSalt(SecurityUtil.generateSalt());
        user.setPassword(SecurityUtil.generateHash(user.getSalt(), user.getPassword()));
        if(user.getDateJoined() == null) {
            user.setDateJoined(new Date());
        }
        int id = userDao.create(user);
        URI uri = uriInfo.getAbsolutePathBuilder().path(String.valueOf(id)).build();
        return Response.created(uri).build();
    }
    
    @POST
    @Path("/{id}/todo")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createTodoForUser(@PathParam("id") int id, Todo todo) {
        System.out.println("Test:" + id);
        System.out.println("Test2:" + todo.getOwner());
        User user = userDao.readById(id);
        if (user == null) {
            throw new NotFoundException();
        }
        todo.setOwner(user);
        if(todo.getDateCreated() == null) {
            todo.setDateCreated(new Date());
        }
        int todoId = todoDao.create(todo);
        URI uri = uriInfo.getBaseUriBuilder().path("todo/" + String.valueOf(todoId)).build();
        return Response.created(uri).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateUser(@PathParam("id") int id, User user) {
        User currentUser = userDao.readById(id);
        if(currentUser == null) {
            throw new NotFoundException();
        }
        // Set the salt manually since we don't allow the user to update it
        user.setSalt(currentUser.getSalt());
        userDao.update(user);
        return Response.noContent().build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteUser(@PathParam("id") int id) {
        userDao.delete(id);
        return Response.noContent().build();
    }
}
