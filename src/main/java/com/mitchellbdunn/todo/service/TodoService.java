package com.mitchellbdunn.todo.service;

import com.mitchellbdunn.todo.dao.TodoDao;
import com.mitchellbdunn.todo.model.Todo;
import java.net.URI;
import java.util.Date;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Mitch Dunn
 */
@Path("/todo")
public class TodoService {

    private final TodoDao dao;
    
    @Context
    UriInfo uriInfo;

    public TodoService() {
        dao = new TodoDao();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response readAllTodos() {
        List<Todo> todos = dao.readAll();
        return Response.ok(todos).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response readTodo(@PathParam("id") int id) {
        Todo todo = dao.readById(id);
        if (todo == null) {
            throw new NotFoundException();
        }
        return Response.ok(todo).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createTodo(Todo todo) {
        if(todo.getDateCreated() == null) {
            todo.setDateCreated(new Date());
        }
        int id = dao.create(todo);
        URI uri = uriInfo.getAbsolutePathBuilder().path(String.valueOf(id)).build();
        return Response.created(uri).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateTodo(@PathParam("id") int id, Todo todo) {
        boolean doesExist = dao.doesEntityExist(id);
        if(!doesExist) {
            throw new NotFoundException();
        }
        dao.update(todo);
        return Response.noContent().build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteTodo(@PathParam("id") int id) {
        dao.delete(id);
        return Response.noContent().build();
    }
}
